package com.untitledkingdom.bluetoothtrainer.feature

import com.tomcz.ellipse.test.processorTest
import com.untitledkingdom.bluetoothtrainer.BaseCoroutineTest
import com.untitledkingdom.bluetoothtrainer.database.Database
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.ReadingsHistoryRepositoryImpl
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.ReadingsHistoryViewModel
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.data.DailyReadingsInfo
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.data.WeatherReading
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.state.ReadingsHistoryEffect
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.state.ReadingsHistoryEvent
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.state.ReadingsHistoryState
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flowOf
import org.junit.Test

@ExperimentalCoroutinesApi
class ReadingsHistoryViewModelTest : BaseCoroutineTest() {
    private val dataBase by lazy { mockk<Database>() }
    private val repository by lazy { ReadingsHistoryRepositoryImpl(database = dataBase) }
    private val viewModel by lazy { ReadingsHistoryViewModel(repository = repository) }

    private val dates = listOf("2022-01-02", "2022-01-02", "2022-01-01", "2022-01-01")
    private val time = "10:00"

    private val weatherReadings = (0..3).map {
        WeatherReading(
            id = it,
            temperature = it.toFloat(),
            humidity = it,
            date = dates[it],
            time = time,
        )
    }

    private val dailyReadingsInfo = listOf(
        DailyReadingsInfo(
            date = "2022-01-01",
            minTemperature = 2f,
            maxTemperature = 3f,
            avgTemperature = 2.5f,
            minHumidity = 2,
            maxHumidity = 3,
            avgHumidity = 2.5f
        ),
        DailyReadingsInfo(
            date = "2022-01-02",
            minTemperature = 0f,
            maxTemperature = 1f,
            avgTemperature = 0.5f,
            minHumidity = 0,
            maxHumidity = 1,
            avgHumidity = 0.5f
        ),
    )

    @Test
    fun `when fetch weatherReadings from repository converts to dailyReadingsInfo and sorts it`() =
        processorTest(
            processor = { viewModel.processor },
            given = {
                coEvery { dataBase.getWeatherDao().getWeatherReadings() } returns
                    flowOf(weatherReadings)
            },
            thenStates = {
                assertLast(
                    ReadingsHistoryState(
                        dailyReadingsInfoList = dailyReadingsInfo
                    )
                )
            }
        )

    @Test
    fun `when item click then GoToWeatherReadings effect`() = processorTest(
        processor = { viewModel.processor },
        given = {
            coEvery { dataBase.getWeatherDao().getWeatherReadings() } returns emptyFlow()
        },
        whenEvent = ReadingsHistoryEvent.ItemClick(date = dates[0]),
        thenEffects = {
            assertValues(
                ReadingsHistoryEffect.GoToWeatherReadingsDetails(date = dates[0])
            )
        }
    )
}
