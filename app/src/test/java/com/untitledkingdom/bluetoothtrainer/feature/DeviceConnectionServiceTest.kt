package com.untitledkingdom.bluetoothtrainer.feature

import com.tomcz.ellipse.test.processorTest
import com.untitledkingdom.bluetoothtrainer.BaseCoroutineTest
import com.untitledkingdom.bluetoothtrainer.api.ApiService
import com.untitledkingdom.bluetoothtrainer.database.Database
import com.untitledkingdom.bluetoothtrainer.datastore.DataStorage
import com.untitledkingdom.bluetoothtrainer.service.DeviceConnectionProcessorContainer
import com.untitledkingdom.bluetoothtrainer.service.state.DeviceConnectionEffect
import com.untitledkingdom.bluetoothtrainer.utils.peripheral.BatteryLevel
import com.untitledkingdom.bluetoothtrainer.utils.peripheral.PeripheralProvider
import io.mockk.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flowOf
import org.junit.Test

@ExperimentalCoroutinesApi
class DeviceConnectionServiceTest : BaseCoroutineTest() {
    private val dataStorage by lazy { mockk<DataStorage>() }
    private val peripheralProvider by lazy { mockk<PeripheralProvider>() }
    private val database by lazy { mockk<Database>() }
    private val apiService by lazy { mockk<ApiService>() }
    private val processorContainer by lazy {
        DeviceConnectionProcessorContainer(
            database = database,
            dataStorage = dataStorage,
            peripheralProvider = peripheralProvider,
            apiService = apiService,
        )
    }

    @Test
    fun `when battery level is low then send notification effect`() = processorTest(
        processor = { processorContainer.processor },
        given = {
            coEvery { database.getWeatherDao().getLatestWeatherReadingsPackage() } returns flowOf()
            coEvery { peripheralProvider.batteryLevel } returns MutableStateFlow(BatteryLevel.Low)
        },
        thenEffects = {
            assertValues(DeviceConnectionEffect.ShowLowBatteryNotification)
        }
    )

    @Test
    fun `when battery level is high then cancel notification effect`() = processorTest(
        processor = { processorContainer.processor },
        given = {
            coEvery { database.getWeatherDao().getLatestWeatherReadingsPackage() } returns flowOf()
            coEvery { peripheralProvider.batteryLevel } returns MutableStateFlow(BatteryLevel.High)
        },
        thenEffects = {
            assertValues(DeviceConnectionEffect.CancelLowBatteryNotification)
        }
    )
}
