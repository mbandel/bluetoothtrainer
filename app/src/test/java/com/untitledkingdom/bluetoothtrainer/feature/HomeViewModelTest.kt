package com.untitledkingdom.bluetoothtrainer.feature

import com.tomcz.ellipse.common.previewProcessor
import com.tomcz.ellipse.test.processorTest
import com.untitledkingdom.bluetoothtrainer.BaseCoroutineTest
import com.untitledkingdom.bluetoothtrainer.api.ConnectionState
import com.untitledkingdom.bluetoothtrainer.api.FakeInternetConnection
import com.untitledkingdom.bluetoothtrainer.bluetooth.BluetoothScanner
import com.untitledkingdom.bluetoothtrainer.datastore.DataStorage
import com.untitledkingdom.bluetoothtrainer.feature.home.HomeViewModel
import com.untitledkingdom.bluetoothtrainer.feature.home.data.AdvertisementView
import com.untitledkingdom.bluetoothtrainer.feature.home.state.HomeEffect
import com.untitledkingdom.bluetoothtrainer.feature.home.state.HomeEvent
import com.untitledkingdom.bluetoothtrainer.feature.home.state.HomeState
import com.untitledkingdom.bluetoothtrainer.service.DeviceConnectionProcessorContainer
import com.untitledkingdom.bluetoothtrainer.utils.Constants
import com.untitledkingdom.bluetoothtrainer.utils.peripheral.PeripheralProvider
import com.untitledkingdom.bluetoothtrainer.utils.peripheral.PeripheralState
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flowOf
import org.junit.Test

@ExperimentalCoroutinesApi
internal class HomeViewModelTest : BaseCoroutineTest() {
    private val bluetoothScanner by lazy { mockk<BluetoothScanner>() }
    private val dataStorage by lazy { mockk<DataStorage>() }
    private val peripheralProvider by lazy { mockk<PeripheralProvider>() }
    private val processorContainer by lazy { mockk<DeviceConnectionProcessorContainer>() }
    private val fakeInternetConnection by lazy { mockk<FakeInternetConnection>() }
    private val viewModel by lazy {
        HomeViewModel(
            bluetoothScanner = bluetoothScanner,
            dataStorage = dataStorage,
            peripheralProvider = peripheralProvider,
            processorContainer = processorContainer,
            fakeInternetConnection = fakeInternetConnection
        )
    }

    private val advertisements = (0..3).map {
        AdvertisementView(
            address = it.toString(),
            rssi = it,
            name = it.toString()
        )
    }
    private val advertisementFlow = flowOf(
        AdvertisementView("0", 0, "0"),
        AdvertisementView("1", 1, "1"),
        AdvertisementView("2", 2, "2"),
        AdvertisementView("3", 3, "not 3"),
        AdvertisementView("3", 3, "3"),
        AdvertisementView("address", 1, "no name")
    )
    private val address = "address"

    @Test
    fun `when location status true then update location status`() = processorTest(
        processor = { viewModel.processor },
        given = {
            coEvery { bluetoothScanner.scanAdvertisements() } returns emptyFlow()
            coEvery { dataStorage.observeAddress(key = Constants.ADDRESS) } returns emptyFlow()
            coEvery { peripheralProvider.state } returns
                MutableStateFlow(PeripheralState.Disconnected)
            coEvery { fakeInternetConnection.state } returns MutableStateFlow(ConnectionState.On)
        },
        whenEvent = HomeEvent.UpdateLocationStatus(true),
        thenStates = {
            assertLast(
                HomeState(
                    isLocationEnabled = true,
                )
            )
        }
    )

    @Test
    fun `when bluetooth status true then update bluetooth status`() = processorTest(
        processor = { viewModel.processor },
        given = {
            coEvery { bluetoothScanner.scanAdvertisements() } returns emptyFlow()
            coEvery { dataStorage.observeAddress(key = Constants.ADDRESS) } returns emptyFlow()
            coEvery { peripheralProvider.state } returns
                MutableStateFlow(PeripheralState.Disconnected)
            coEvery { fakeInternetConnection.state } returns MutableStateFlow(ConnectionState.On)
        },
        whenEvent = HomeEvent.UpdateBluetoothStatus(true),
        thenStates = {
            assertLast(HomeState(isBluetoothEnabled = true))
        }
    )

    @Test
    fun `when fake Internet Connection status off isFakeInternetEnabled = false`() = processorTest(
        processor = { viewModel.processor },
        given = {
            coEvery { bluetoothScanner.scanAdvertisements() } returns emptyFlow()
            coEvery { dataStorage.observeAddress(key = Constants.ADDRESS) } returns emptyFlow()
            coEvery { peripheralProvider.state } returns
                MutableStateFlow(PeripheralState.Disconnected)
            coEvery { fakeInternetConnection.state } returns MutableStateFlow(ConnectionState.Off)
            coEvery { fakeInternetConnection.turnOff() } returns Unit
        },
        thenStates = {
            assertLast(HomeState(isFakeInternetEnabled = false))
        }
    )

    @Test
    fun `when start scanning update advertisements`() = processorTest(
        processor = { viewModel.processor },
        given = {
            coEvery { bluetoothScanner.scanAdvertisements() } returns advertisementFlow
            coEvery { dataStorage.observeAddress(key = Constants.ADDRESS) } returns emptyFlow()
            coEvery { peripheralProvider.state } returns
                MutableStateFlow(PeripheralState.Disconnected)
            coEvery { fakeInternetConnection.state } returns MutableStateFlow(ConnectionState.On)
        },
        whenEvent = HomeEvent.StartScanningClick,
        thenStates = {
            assertLast(HomeState(advertisements = advertisements))
        }
    )

    @Test
    fun `when advertisement click then go to ReadingsHistory`() = processorTest(
        processor = { viewModel.processor },
        given = {
            coEvery { bluetoothScanner.scanAdvertisements() } returns emptyFlow()
            coEvery { dataStorage.observeAddress(key = Constants.ADDRESS) } returns emptyFlow()
            coEvery { peripheralProvider.state } returns
                MutableStateFlow(PeripheralState.Disconnected)
            coEvery { fakeInternetConnection.state } returns MutableStateFlow(ConnectionState.On)
            coEvery {
                dataStorage.save(
                    key = Constants.ADDRESS,
                    value = address
                )
            } returns Unit
            coEvery { processorContainer.processor } returns previewProcessor(Unit)
        },
        whenEvent = HomeEvent.AdvertisementClick(address = address),
        thenEffects = {
            assertValues(HomeEffect.GoToReadingsHistory(address = address))
        }
    )
}
