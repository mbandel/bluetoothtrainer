package com.untitledkingdom.bluetoothtrainer.feature

import com.tomcz.ellipse.test.processorTest
import com.untitledkingdom.bluetoothtrainer.BaseCoroutineTest
import com.untitledkingdom.bluetoothtrainer.database.Database
import com.untitledkingdom.bluetoothtrainer.feature.readingsDetails.ReadingsDetailsViewModel
import com.untitledkingdom.bluetoothtrainer.feature.readingsDetails.state.ReadingsDetailsEvent
import com.untitledkingdom.bluetoothtrainer.feature.readingsDetails.state.ReadingsDetailsState
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.data.WeatherReading
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.flowOf
import org.junit.Test

@ExperimentalCoroutinesApi
@FlowPreview
class ReadingsDetailsViewModelTest : BaseCoroutineTest() {
    private val dataBase by lazy { mockk<Database>() }
    private val viewModel by lazy { ReadingsDetailsViewModel(database = dataBase) }

    private val date = "2022-01-01"
    private val time = "12:00"
    private val weatherReadings = (0..3).map {
        WeatherReading(
            id = it,
            temperature = it.toFloat(),
            humidity = it,
            date = date,
            time = time,
        )
    }

    @Test
    fun `when fetchWeatherReadings update weatherReadings`() = processorTest(
        processor = { viewModel.processor },
        given = {
            coEvery { dataBase.getWeatherDao().getWeatherReadingsByDate(date = date) } returns
                flowOf(weatherReadings)
        },
        whenEvent = ReadingsDetailsEvent.FetchWeatherReadings(date = date),
        thenStates = {
            assertLast(
                ReadingsDetailsState(
                    weatherReadings = weatherReadings,
                    date = date
                )
            )
        }
    )
}
