package com.untitledkingdom.bluetoothtrainer.feature

import com.untitledkingdom.bluetoothtrainer.utils.ByteConverter
import org.junit.Test
import java.time.LocalDate

class ByteConverterTest {
    @Test
    fun `check if byte conversion is correct`() {
        val date = LocalDate.of(2022, 1, 1)
        val byteArray = ByteConverter.getByteArrayFromDate(date)
        assert(ByteConverter.getDateFromByteArray(byteArray) == date)
    }
}
