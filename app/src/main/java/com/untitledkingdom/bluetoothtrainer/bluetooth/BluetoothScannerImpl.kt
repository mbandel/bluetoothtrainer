package com.untitledkingdom.bluetoothtrainer.bluetooth

import com.juul.kable.Scanner
import com.untitledkingdom.bluetoothtrainer.feature.home.data.AdvertisementView
import com.untitledkingdom.bluetoothtrainer.feature.home.data.toAdvertisementView
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class BluetoothScannerImpl @Inject constructor() : BluetoothScanner {
    override fun scanAdvertisements(): Flow<AdvertisementView> =
        Scanner().advertisements.map { advertisement ->
            advertisement.toAdvertisementView()
        }
}
