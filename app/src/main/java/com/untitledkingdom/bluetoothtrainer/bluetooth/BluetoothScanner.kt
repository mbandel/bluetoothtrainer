package com.untitledkingdom.bluetoothtrainer.bluetooth

import com.untitledkingdom.bluetoothtrainer.feature.home.data.AdvertisementView
import kotlinx.coroutines.flow.Flow

interface BluetoothScanner {
    fun scanAdvertisements(): Flow<AdvertisementView>
}
