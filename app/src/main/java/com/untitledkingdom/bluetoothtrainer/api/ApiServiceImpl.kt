package com.untitledkingdom.bluetoothtrainer.api

import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.data.WeatherReading
import kotlinx.coroutines.delay
import okhttp3.ResponseBody.Companion.toResponseBody
import retrofit2.Response
import javax.inject.Inject

class ApiServiceImpl @Inject constructor(
    private val fakeInternetConnection: FakeInternetConnection
) : ApiService {
    override suspend fun sendWeatherReadings(
        weatherReadings: List<WeatherReading>
    ): Response<Unit> {
        delay(2000L)
        return if (fakeInternetConnection.state.value == ConnectionState.On)
            Response.success(Unit)
        else Response.error(500, "".toResponseBody())
    }
}
