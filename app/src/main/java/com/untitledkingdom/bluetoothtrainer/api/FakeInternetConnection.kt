package com.untitledkingdom.bluetoothtrainer.api

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FakeInternetConnection @Inject constructor() {
    private var _state: MutableStateFlow<ConnectionState> = MutableStateFlow(ConnectionState.On)
    val state: StateFlow<ConnectionState>
        get() = _state

    fun turnOn() {
        _state.value = ConnectionState.On
    }

    fun turnOff() {
        _state.value = ConnectionState.Off
    }
}
