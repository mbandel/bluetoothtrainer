package com.untitledkingdom.bluetoothtrainer.api

sealed class ConnectionState {
    object On : ConnectionState()
    object Off : ConnectionState()
}
