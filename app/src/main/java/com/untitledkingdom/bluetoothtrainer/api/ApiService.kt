package com.untitledkingdom.bluetoothtrainer.api

import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.data.WeatherReading
import retrofit2.Response

interface ApiService {
    suspend fun sendWeatherReadings(
        weatherReadings: List<WeatherReading>
    ): Response<Unit>
}
