package com.untitledkingdom.bluetoothtrainer.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.FlowPreview

@DelicateCoroutinesApi
@FlowPreview
class ServiceCancelReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == DeviceConnectionService.ACTION_CANCEL_WORK) {
            context.stopService(Intent(context, DeviceConnectionService::class.java))
        }
    }
}
