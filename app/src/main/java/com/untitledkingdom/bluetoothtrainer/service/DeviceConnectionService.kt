package com.untitledkingdom.bluetoothtrainer.service

import android.app.* // ktlint-disable no-wildcard-imports
import android.content.Context
import android.content.Intent
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.tomcz.ellipse.common.onProcessor
import com.untitledkingdom.bluetoothtrainer.R
import com.untitledkingdom.bluetoothtrainer.service.state.DeviceConnectionEffect
import com.untitledkingdom.bluetoothtrainer.service.state.DeviceConnectionEvent
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports
import javax.inject.Inject

@DelicateCoroutinesApi
@FlowPreview
@AndroidEntryPoint
class DeviceConnectionService : Service() {
    @Inject
    internal lateinit var processorContainer: DeviceConnectionProcessorContainer

    private val scope: CoroutineScope = CoroutineScope(
        SupervisorJob() + Dispatchers.Main.immediate
    )
    private val notificationManager by lazy { NotificationManagerCompat.from(this) }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        scope.onProcessor(
            processor = { processorContainer.processor },
            onEffect = ::trigger
        )
        startForeground(STICKY_NOTIFICATION_ID, createForegroundInfo())
        initializePeripheral()
        return START_NOT_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    private fun trigger(effect: DeviceConnectionEffect) {
        when (effect) {
            DeviceConnectionEffect.ShowLowBatteryNotification -> showLowBatteryNotification()
            DeviceConnectionEffect.CancelLowBatteryNotification -> cancelBatteryNotification()
        }
    }

    private fun initializePeripheral() {
        processorContainer.processor.sendEvent(DeviceConnectionEvent.InitializePeripheral)
    }

    override fun onDestroy() {
        processorContainer.processor.sendEvent(DeviceConnectionEvent.DisconnectPeripheral)
        processorContainer.processor.sendEvent(DeviceConnectionEvent.CancelScope)
        scope.coroutineContext.cancelChildren()
        super.onDestroy()
    }

    private fun cancelWorkIntent(): PendingIntent {
        val intent = Intent(applicationContext, ServiceCancelReceiver::class.java).also {
            it.action = ACTION_CANCEL_WORK
        }
        return PendingIntent.getBroadcast(
            applicationContext,
            0,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_MUTABLE
        )
    }

    private fun createForegroundInfo(): Notification {
        createChannel(
            name = STICKY_NOTIFICATION_NAME,
            channelId = STICKY_NOTIFICATION_CHANNEL_ID
        )
        return NotificationCompat.Builder(
            applicationContext,
            STICKY_NOTIFICATION_CHANNEL_ID
        )
            .setContentTitle(applicationContext.getString(R.string.collecting_data))
            .setSmallIcon(R.drawable.ic_bluetooth_on)
            .setOngoing(false)
            .addAction(
                android.R.drawable.ic_delete,
                ACTION_CANCEL_WORK, cancelWorkIntent()
            )
            .build()
    }

    private fun createChannel(
        name: String,
        channelId: String
    ) {
        val importance = NotificationManager.IMPORTANCE_HIGH
        val channel = NotificationChannel(
            channelId, name, importance
        )
        val notificationManager: NotificationManager = applicationContext
            .getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }

    private fun showLowBatteryNotification() {
        createChannel(
            name = BATTERY_NOTIFICATION_NAME,
            channelId = BATTERY_NOTIFICATION_CHANNEL_ID
        )
        val batteryNotification = NotificationCompat.Builder(
            applicationContext,
            BATTERY_NOTIFICATION_CHANNEL_ID
        )
            .setContentTitle(getString(R.string.low_battery_warning))
            .setSmallIcon(R.drawable.ic_bluetooth_on)
            .setOngoing(false)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .build()

        notificationManager.notify(BATTERY_NOTIFICATION_ID, batteryNotification)
    }

    private fun cancelBatteryNotification() {
        notificationManager.cancel(BATTERY_NOTIFICATION_ID)
    }

    companion object {
        const val STICKY_NOTIFICATION_CHANNEL_ID = "1"
        const val STICKY_NOTIFICATION_ID = 1
        const val BATTERY_NOTIFICATION_CHANNEL_ID = "2"
        const val BATTERY_NOTIFICATION_ID = 2
        const val ACTION_CANCEL_WORK = "cancel"
        const val STICKY_NOTIFICATION_NAME = "sticky notification"
        const val BATTERY_NOTIFICATION_NAME = "battery notification"
    }
}
