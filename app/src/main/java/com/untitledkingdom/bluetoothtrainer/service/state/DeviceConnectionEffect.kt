package com.untitledkingdom.bluetoothtrainer.service.state

sealed interface DeviceConnectionEffect {
    object ShowLowBatteryNotification : DeviceConnectionEffect
    object CancelLowBatteryNotification : DeviceConnectionEffect
}
