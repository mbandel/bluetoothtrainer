package com.untitledkingdom.bluetoothtrainer.service

import com.tomcz.ellipse.EffectsCollector
import com.tomcz.ellipse.Processor
import com.tomcz.ellipse.common.processor
import com.untitledkingdom.bluetoothtrainer.api.ApiService
import com.untitledkingdom.bluetoothtrainer.database.Database
import com.untitledkingdom.bluetoothtrainer.datastore.DataStorage
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.data.WeatherReading
import com.untitledkingdom.bluetoothtrainer.service.state.DeviceConnectionEffect
import com.untitledkingdom.bluetoothtrainer.service.state.DeviceConnectionEvent
import com.untitledkingdom.bluetoothtrainer.utils.Constants.ADDRESS
import com.untitledkingdom.bluetoothtrainer.utils.Constants.SYNC_PACKAGE_SIZE
import com.untitledkingdom.bluetoothtrainer.utils.peripheral.BatteryLevel
import com.untitledkingdom.bluetoothtrainer.utils.peripheral.PeripheralProvider
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports
import javax.inject.Inject
import javax.inject.Singleton

typealias DeviceConnectionProcessor = Processor<DeviceConnectionEvent, Unit, DeviceConnectionEffect>

@Singleton
class DeviceConnectionProcessorContainer @Inject constructor(
    private val dataStorage: DataStorage,
    private val peripheralProvider: PeripheralProvider,
    private val database: Database,
    private val apiService: ApiService,
) {
    private val scope: CoroutineScope = CoroutineScope(
        SupervisorJob() + Dispatchers.Main.immediate
    )

    val processor: DeviceConnectionProcessor = scope.processor(
        prepare = {
            observeBatteryLevel(effects = effects)
            observeUnsynchronizedReadings()
        },
        onEvent = { event ->
            when (event) {
                DeviceConnectionEvent.InitializePeripheral -> initializePeripheral()
                DeviceConnectionEvent.DisconnectPeripheral -> disconnectPeripheral()
                DeviceConnectionEvent.CancelScope -> cancelScope()
            }
        }
    )

    private suspend fun disconnectPeripheral() {
        peripheralProvider.setDisconnected()
        peripheralProvider.cancelScope()
    }

    private fun cancelScope() {
        scope.coroutineContext.cancelChildren()
    }

    private suspend fun initializePeripheral() {
        val address = dataStorage.read(key = ADDRESS)
        if (address != "")
            peripheralProvider.init(address = address)
    }

    private fun observeUnsynchronizedReadings() = scope.launch {
        database.getWeatherDao().getLatestWeatherReadingsPackage()
            .collect { weatherReadings ->
                if (weatherReadings.size == SYNC_PACKAGE_SIZE) {
                    sendData(weatherReadings = weatherReadings)
                }
            }
    }

    private suspend fun sendData(weatherReadings: List<WeatherReading>) {
        println("send data: ${weatherReadings.map { it.id }}")
        val response = apiService.sendWeatherReadings(weatherReadings)
        if (response.isSuccessful) {
            database.getWeatherDao().addWeatherReadingList(
                weatherReadings = weatherReadings.map { it.copy(isSentToServer = true) }
            )
        }
    }

    private fun observeBatteryLevel(effects: EffectsCollector<DeviceConnectionEffect>) =
        scope.launch {
            peripheralProvider.batteryLevel.collect { batteryLevel ->
                when (batteryLevel) {
                    BatteryLevel.Low -> effects.send(
                        DeviceConnectionEffect.ShowLowBatteryNotification
                    )
                    BatteryLevel.High -> effects.send(
                        DeviceConnectionEffect.CancelLowBatteryNotification
                    )
                }
            }
        }
}
