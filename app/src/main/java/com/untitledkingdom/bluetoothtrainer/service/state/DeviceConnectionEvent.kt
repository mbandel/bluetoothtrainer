package com.untitledkingdom.bluetoothtrainer.service.state

sealed interface DeviceConnectionEvent {
    object DisconnectPeripheral : DeviceConnectionEvent
    object InitializePeripheral : DeviceConnectionEvent
    object CancelScope : DeviceConnectionEvent
}
