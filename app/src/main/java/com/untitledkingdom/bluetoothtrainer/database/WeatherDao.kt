package com.untitledkingdom.bluetoothtrainer.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.data.WeatherReading
import com.untitledkingdom.bluetoothtrainer.utils.Constants
import kotlinx.coroutines.flow.Flow

@Dao
interface WeatherDao {
    @Query("SELECT * FROM ${Constants.WEATHER_TABLE}")
    fun getWeatherReadings(): Flow<List<WeatherReading>>

    @Query("SELECT * FROM ${Constants.WEATHER_TABLE} WHERE date = :date")
    fun getWeatherReadingsByDate(date: String): Flow<List<WeatherReading>>

    @Query(
        "SELECT * FROM ${Constants.WEATHER_TABLE} " +
            "WHERE isSentToServer = 0 " +
            "LIMIT ${Constants.SYNC_PACKAGE_SIZE}"
    )
    fun getLatestWeatherReadingsPackage(): Flow<List<WeatherReading>>

    @Insert(onConflict = REPLACE)
    suspend fun addWeatherReading(weatherReading: WeatherReading)

    @Insert(onConflict = REPLACE)
    suspend fun addWeatherReadingList(weatherReadings: List<WeatherReading>)
}
