package com.untitledkingdom.bluetoothtrainer.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.data.WeatherReading

@Database(
    entities = [WeatherReading::class],
    version = 3,
    exportSchema = false
)
abstract class Database : RoomDatabase() {
    abstract fun getWeatherDao(): WeatherDao
}
