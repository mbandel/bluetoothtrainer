package com.untitledkingdom.bluetoothtrainer.datastore

import kotlinx.coroutines.flow.Flow

interface DataStorage {
    suspend fun save(key: String, value: String)
    suspend fun read(key: String): String
    fun observeAddress(key: String): Flow<String>
}
