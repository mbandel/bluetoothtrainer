package com.untitledkingdom.bluetoothtrainer.datastore

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class DataStorageImpl @Inject constructor(
    private val dataStore: DataStore<Preferences>
) : DataStorage {
    override suspend fun save(key: String, value: String) {
        dataStore.edit { dataStore ->
            dataStore[stringPreferencesKey(key)] = value
        }
    }

    override suspend fun read(key: String): String =
        dataStore.data
            .map { dataStore ->
                dataStore[stringPreferencesKey(key)] ?: ""
            }
            .first()

    override fun observeAddress(key: String): Flow<String> =
        dataStore.data.map { dataStore ->
            dataStore[stringPreferencesKey(key)] ?: ""
        }
}
