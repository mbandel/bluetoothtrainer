package com.untitledkingdom.bluetoothtrainer.ui

import androidx.compose.ui.unit.sp

object FontSize {
    val medium = 16.sp
    val large = 20.sp
}
