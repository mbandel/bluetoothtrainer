package com.untitledkingdom.bluetoothtrainer.ui

import androidx.compose.ui.graphics.Color

object Colors {
    val gray = Color(0xFF787878)
    val lightGray = Color(0xFFC8C8C8)
}
