package com.untitledkingdom.bluetoothtrainer

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.untitledkingdom.bluetoothtrainer.service.DeviceConnectionService
import com.untitledkingdom.bluetoothtrainer.service.ServiceCancelReceiver
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.FlowPreview

@AndroidEntryPoint
@FlowPreview
@DelicateCoroutinesApi
@RequiresApi(Build.VERSION_CODES.S)
class MainActivity : AppCompatActivity() {
    private val bluetoothAdapter: BluetoothAdapter by lazy {
        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothManager.adapter
    }
    private val serviceCancelReceiver by lazy { ServiceCancelReceiver() }
    private val bluetoothBroadcastReceiver by lazy { provideBluetoothBroadcastReceiver() }
    private var isServiceStarted = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        askForBluetoothPermission()
        registerReceiver(
            serviceCancelReceiver,
            IntentFilter(DeviceConnectionService.ACTION_CANCEL_WORK)
        )
        registerReceiver(
            bluetoothBroadcastReceiver,
            IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        )
        if (bluetoothAdapter.isEnabled && !isServiceStarted) {
            startDeviceConnectionService()
        } else {
            askForBluetoothEnablement()
        }
    }

    private fun startDeviceConnectionService() {
        isServiceStarted = true
        startForegroundService(getServiceIntent())
    }

    override fun onDestroy() {
        stopService(getServiceIntent())
        unregisterReceiver(serviceCancelReceiver)
        unregisterReceiver(bluetoothBroadcastReceiver)
        super.onDestroy()
    }

    private fun getServiceIntent() = Intent(this, DeviceConnectionService::class.java)

    private fun askForBluetoothPermission() {
        val permissionRequest = registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { }
        permissionRequest.launch(
            arrayOf(
                Manifest.permission.BLUETOOTH_CONNECT,
                Manifest.permission.BLUETOOTH_SCAN,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        )
    }

    private fun provideBluetoothBroadcastReceiver(): BroadcastReceiver =
        object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                val action: String? = intent?.action
                if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                    val state: Int? =
                        intent?.getIntExtra(
                            BluetoothAdapter.EXTRA_STATE,
                            BluetoothAdapter.ERROR
                        )
                    when (state) {
                        BluetoothAdapter.STATE_ON -> {
                            if (!isServiceStarted)
                                startDeviceConnectionService()
                        }
                        BluetoothAdapter.STATE_OFF -> {
                            askForBluetoothEnablement()
                        }
                    }
                }
            }
        }

    private fun askForBluetoothEnablement() {
        val enableBluetoothCode = 55001
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.BLUETOOTH_CONNECT
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            startActivityForResult(
                Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),
                enableBluetoothCode
            )
        }
    }
}
