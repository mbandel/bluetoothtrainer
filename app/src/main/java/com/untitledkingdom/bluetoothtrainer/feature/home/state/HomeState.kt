package com.untitledkingdom.bluetoothtrainer.feature.home.state

import com.untitledkingdom.bluetoothtrainer.feature.home.data.AdvertisementView

data class HomeState(
    val isFakeInternetEnabled: Boolean = true,
    val isBluetoothEnabled: Boolean = false,
    val isLocationEnabled: Boolean = false,
    val isDeviceConnected: Boolean = false,
    val advertisements: List<AdvertisementView> = emptyList(),
    val connectedPeripheralAddress: String = ""
)
