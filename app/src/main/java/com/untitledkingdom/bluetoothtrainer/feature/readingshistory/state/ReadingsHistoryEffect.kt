package com.untitledkingdom.bluetoothtrainer.feature.readingshistory.state

sealed interface ReadingsHistoryEffect {
    data class GoToWeatherReadingsDetails(val date: String) : ReadingsHistoryEffect
}
