package com.untitledkingdom.bluetoothtrainer.feature.readingsDetails.state

sealed interface ReadingsDetailsEvent {
    data class FetchWeatherReadings(val date: String) : ReadingsDetailsEvent
}
