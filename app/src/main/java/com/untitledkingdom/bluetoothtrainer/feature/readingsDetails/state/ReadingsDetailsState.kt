package com.untitledkingdom.bluetoothtrainer.feature.readingsDetails.state

import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.data.WeatherReading

data class ReadingsDetailsState(
    val date: String = "",
    val weatherReadings: List<WeatherReading> = listOf()
)
