package com.untitledkingdom.bluetoothtrainer.feature.home.state

sealed interface HomeEffect {
    data class GoToReadingsHistory(val address: String) : HomeEffect
    object AskForPermissions : HomeEffect
}
