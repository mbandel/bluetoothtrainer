package com.untitledkingdom.bluetoothtrainer.feature.readingshistory

import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.data.DailyReadingsInfo
import kotlinx.coroutines.flow.Flow

interface ReadingsHistoryRepository {
    fun observeReadingsInfo(): Flow<List<DailyReadingsInfo>>
}
