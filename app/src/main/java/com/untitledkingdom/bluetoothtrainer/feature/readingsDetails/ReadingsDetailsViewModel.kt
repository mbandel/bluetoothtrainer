package com.untitledkingdom.bluetoothtrainer.feature.readingsDetails

import androidx.lifecycle.ViewModel
import com.tomcz.ellipse.PartialState
import com.tomcz.ellipse.Processor
import com.tomcz.ellipse.common.processor
import com.untitledkingdom.bluetoothtrainer.database.Database
import com.untitledkingdom.bluetoothtrainer.feature.readingsDetails.state.ReadingsDetailsEffect
import com.untitledkingdom.bluetoothtrainer.feature.readingsDetails.state.ReadingsDetailsEvent
import com.untitledkingdom.bluetoothtrainer.feature.readingsDetails.state.ReadingsDetailsPartialState
import com.untitledkingdom.bluetoothtrainer.feature.readingsDetails.state.ReadingsDetailsState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.* // ktlint-disable no-wildcard-imports
import javax.inject.Inject

typealias ReadingsDetailsProcessor =
    Processor<ReadingsDetailsEvent, ReadingsDetailsState, ReadingsDetailsEffect>

@HiltViewModel
@FlowPreview
class ReadingsDetailsViewModel @Inject constructor(
    private val database: Database
) : ViewModel() {
    val processor: ReadingsDetailsProcessor = processor(
        initialState = ReadingsDetailsState(),
        onEvent = { event ->
            when (event) {
                is ReadingsDetailsEvent.FetchWeatherReadings -> merge(
                    fetchWeatherReadingsByDate(date = event.date),
                    flowOf(ReadingsDetailsPartialState.SetDate(date = event.date))
                )
            }
        }
    )

    private fun fetchWeatherReadingsByDate(date: String): Flow<PartialState<ReadingsDetailsState>> =
        database.getWeatherDao().getWeatherReadingsByDate(date = date)
            .map {
                ReadingsDetailsPartialState.WeatherReadingsChanged(weatherReadings = it)
            }
}
