package com.untitledkingdom.bluetoothtrainer.feature.home

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.* // ktlint-disable no-wildcard-imports
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.Switch
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.tomcz.ellipse.common.collectAsState
import com.tomcz.ellipse.common.previewProcessor
import com.untitledkingdom.bluetoothtrainer.R
import com.untitledkingdom.bluetoothtrainer.feature.home.data.AdvertisementView
import com.untitledkingdom.bluetoothtrainer.feature.home.state.HomeEvent
import com.untitledkingdom.bluetoothtrainer.feature.home.state.HomeState

@Composable
fun HomeScreen(processor: HomeProcessor) {
    val isLocationEnabled by processor.collectAsState { it.isLocationEnabled }
    val isBluetoothEnabled by processor.collectAsState { it.isBluetoothEnabled }
    val isDeviceConnected by processor.collectAsState { it.isDeviceConnected }
    val advertisements by processor.collectAsState { it.advertisements }
    val connectedPeripheralAddress by processor.collectAsState { it.connectedPeripheralAddress }

    Column(
        verticalArrangement = Arrangement.spacedBy(12.dp),
        modifier = Modifier.padding(horizontal = 32.dp)
    ) {
        Text(
            text = stringResource(id = R.string.app_info),
            modifier = Modifier.padding(top = 20.dp)
        )
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            FakeInternetConnectionInfo(processor = processor)
            BluetoothAndLocationInfo(
                isBluetoothEnabled = isBluetoothEnabled,
                isLocationEnabled = isLocationEnabled,
                isDeviceConnected = isDeviceConnected
            )
        }

        Button(
            modifier = Modifier
                .padding(
                    bottom = 20.dp,
                    top = 10.dp
                )
                .fillMaxWidth(),
            enabled = isBluetoothEnabled && isLocationEnabled,
            onClick = { processor.sendEvent(HomeEvent.StartScanningClick) }
        ) {
            Text(
                text = stringResource(id = R.string.start_scanning),
                modifier = Modifier
                    .padding(8.dp)
            )
        }
        AdvertisementList(
            address = connectedPeripheralAddress,
            advertisements = advertisements,
            onConnect = { processor.sendEvent(HomeEvent.AdvertisementClick(it.address)) },
            onDisconnect = { processor.sendEvent(HomeEvent.DisconnectPeripheral) }
        )
    }
}

@Composable
private fun FakeInternetConnectionInfo(processor: HomeProcessor) {
    val isFakeInternetEnabled by processor.collectAsState { it.isFakeInternetEnabled }
    val checkedState = remember {
        mutableStateOf(isFakeInternetEnabled)
    }
    Row(
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Switch(
            checked = checkedState.value,
            onCheckedChange = {
                checkedState.value = it
                processor.sendEvent(HomeEvent.UpdateFakeInternetStatus(checkedState.value))
            }
        )
        Image(
            painter = getWifiPainter(isFakeInternetEnabled = isFakeInternetEnabled),
            contentDescription = null,
            modifier = Modifier
                .height(42.dp)
                .width(42.dp)
        )
    }
}

@Composable
private fun BluetoothAndLocationInfo(
    isBluetoothEnabled: Boolean,
    isLocationEnabled: Boolean,
    isDeviceConnected: Boolean
) {
    Row(
        horizontalArrangement = Arrangement.spacedBy(12.dp)
    ) {
        Image(
            painter = getDeviceConnectedPainter(isDeviceConnected = isDeviceConnected),
            contentDescription = null,
            modifier = Modifier
                .height(42.dp)
                .width(42.dp)
        )
        Image(
            painter = getBluetoothPainter(isBluetoothEnabled = isBluetoothEnabled),
            contentDescription = null,
            modifier = Modifier
                .height(42.dp)
                .width(42.dp)
        )
        Image(
            painter = getLocationPainter(isLocationEnabled = isLocationEnabled),
            contentDescription = null,
            modifier = Modifier
                .height(42.dp)
                .width(42.dp)
        )
    }
}

@Composable
private fun AdvertisementList(
    address: String,
    advertisements: List<AdvertisementView>,
    onConnect: (advertisement: AdvertisementView) -> Unit,
    onDisconnect: () -> Unit
) {
    LazyColumn(
        verticalArrangement = Arrangement.spacedBy(2.dp)
    ) {
        items(advertisements) { advertisement ->
            AdvertisementItem(
                address = address,
                advertisement = advertisement,
                onConnect = onConnect,
                onDisconnect = onDisconnect
            )
        }
    }
}

@Composable
private fun AdvertisementItem(
    address: String,
    advertisement: AdvertisementView,
    onConnect: (advertisement: AdvertisementView) -> Unit,
    onDisconnect: () -> Unit,
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier.fillMaxWidth()
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            Column {
                Image(
                    painter = getSignalPainter(rssi = advertisement.rssi),
                    contentDescription = null
                )
                Text(text = advertisement.rssi.toString())
            }
            Column(
                modifier = Modifier.padding(vertical = 24.dp)
            ) {
                Text(text = advertisement.name)
                Text(text = advertisement.address)
            }
        }
        if (address == advertisement.address)
            Button(
                onClick = { onDisconnect() }
            ) {
                Text(text = stringResource(id = R.string.disconnect))
            }
        else
            Button(
                onClick = { onConnect(advertisement) }
            ) {
                Text(text = stringResource(id = R.string.connect))
            }
    }
}

@Composable
private fun getSignalPainter(rssi: Int): Painter {
    return if (rssi < -80)
        painterResource(id = R.drawable.ic_signal_0)
    else if (rssi >= -80 && rssi < -70)
        painterResource(id = R.drawable.ic_signal_1)
    else if (rssi >= -70 && rssi < -55)
        painterResource(id = R.drawable.ic_signal_2)
    else
        painterResource(id = R.drawable.ic_signal_3)
}

@Composable
private fun getDeviceConnectedPainter(isDeviceConnected: Boolean): Painter {
    return if (isDeviceConnected)
        painterResource(id = R.drawable.ic_connected)
    else painterResource(id = R.drawable.ic_disconnected)
}

@Composable
private fun getBluetoothPainter(isBluetoothEnabled: Boolean): Painter {
    return if (isBluetoothEnabled)
        painterResource(id = R.drawable.ic_bluetooth_on)
    else painterResource(id = R.drawable.ic_bluetooth_off)
}

@Composable
private fun getLocationPainter(isLocationEnabled: Boolean): Painter {
    return if (isLocationEnabled)
        painterResource(id = R.drawable.ic_location_on)
    else painterResource(id = R.drawable.ic_location_off)
}

@Composable
private fun getWifiPainter(isFakeInternetEnabled: Boolean): Painter {
    return if (isFakeInternetEnabled)
        painterResource(id = R.drawable.ic_wifi)
    else painterResource(id = R.drawable.ic_wifi_off)
}

@Composable
@Preview(showBackground = true, showSystemUi = true)
private fun Preview() {
    HomeScreen(processor = previewProcessor(HomeState()))
}
