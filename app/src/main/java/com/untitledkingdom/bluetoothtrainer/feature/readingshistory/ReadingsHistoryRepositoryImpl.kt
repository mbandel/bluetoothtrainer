package com.untitledkingdom.bluetoothtrainer.feature.readingshistory

import com.untitledkingdom.bluetoothtrainer.database.Database
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.data.DailyReadingsInfo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class ReadingsHistoryRepositoryImpl @Inject constructor(
    private val database: Database
) : ReadingsHistoryRepository {
    override fun observeReadingsInfo(): Flow<List<DailyReadingsInfo>> =
        database.getWeatherDao().getWeatherReadings()
            .map { readingsInfo ->
                val weatherReadingsByDate = readingsInfo.groupBy { it.date }
                val dates = weatherReadingsByDate.keys
                dates.toList().map { date ->
                    val weatherReadings = weatherReadingsByDate[date]!!
                    DailyReadingsInfo(
                        date = date,
                        minTemperature = weatherReadings.minOf { it.temperature },
                        maxTemperature = weatherReadings.maxOf { it.temperature },
                        avgTemperature = weatherReadings.map { it.temperature }.average().toFloat(),
                        minHumidity = weatherReadings.minOf { it.humidity },
                        maxHumidity = weatherReadings.maxOf { it.humidity },
                        avgHumidity = weatherReadings.map { it.humidity }.average().toFloat()
                    )
                }
            }
}
