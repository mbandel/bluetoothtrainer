package com.untitledkingdom.bluetoothtrainer.feature.readingshistory.data

data class DailyReadingsInfo(
    val date: String,
    val minTemperature: Float,
    val maxTemperature: Float,
    val avgTemperature: Float,
    val minHumidity: Int,
    val maxHumidity: Int,
    val avgHumidity: Float,
)
