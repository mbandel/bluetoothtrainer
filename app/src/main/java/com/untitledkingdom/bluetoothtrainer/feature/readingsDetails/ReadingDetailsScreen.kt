package com.untitledkingdom.bluetoothtrainer.feature.readingsDetails

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.* // ktlint-disable no-wildcard-imports
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.* // ktlint-disable no-wildcard-imports
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.PathEffect
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.madrapps.plot.line.DataPoint
import com.madrapps.plot.line.LineGraph
import com.madrapps.plot.line.LinePlot
import com.tomcz.ellipse.common.collectAsState
import com.tomcz.ellipse.common.previewProcessor
import com.untitledkingdom.bluetoothtrainer.R
import com.untitledkingdom.bluetoothtrainer.feature.readingsDetails.state.ReadingsDetailsState
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.data.WeatherReading
import com.untitledkingdom.bluetoothtrainer.ui.Colors
import com.untitledkingdom.bluetoothtrainer.ui.FontSize
import kotlinx.coroutines.launch

@Composable
fun ReadingDetailsScreen(processor: ReadingsDetailsProcessor) {
    val weatherReadings by processor.collectAsState { it.weatherReadings }
    val date by processor.collectAsState { it.date }
    val listState = rememberLazyListState()
    val xOffset = remember { mutableStateOf(0f) }
    val cardWidth = remember { mutableStateOf(0) }
    val visibility = remember { mutableStateOf(false) }
    val reading = remember { mutableStateOf(listOf<WeatherReading>()) }
    val totalWidth = remember { mutableStateOf(0) }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
            .padding(end = 32.dp)
            .onGloballyPositioned {
                totalWidth.value = it.size.width
            },
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        TitleRow(
            date = date,
            size = weatherReadings.size
        )
        SelectedDataPoint(
            visibility = visibility,
            xOffset = xOffset,
            cardWidth = cardWidth,
            readings = reading,
        )
        Plot(
            weatherReadings = weatherReadings,
            listState = listState,
            visibility = visibility,
            xOffset = xOffset,
            cardWidth = cardWidth,
            reading = reading,
            totalWidth = totalWidth
        )
        HeadersRow()
        WeatherReadingsList(
            weatherReadings = weatherReadings,
            listState = listState
        )
    }
}

@Composable
private fun SelectedDataPoint(
    visibility: MutableState<Boolean>,
    xOffset: MutableState<Float>,
    cardWidth: MutableState<Int>,
    readings: MutableState<List<WeatherReading>>,
) {
    if (visibility.value) {
        Surface(
            modifier = Modifier
                .onGloballyPositioned {
                    cardWidth.value = it.size.width
                }
                .graphicsLayer(translationX = xOffset.value),
            shape = RoundedCornerShape(12.dp),
            color = Colors.lightGray
        ) {
            Column(
                Modifier
                    .padding(
                        horizontal = 12.dp,
                        vertical = 6.dp
                    )
            ) {
                val value = readings.value
                if (value.isNotEmpty()) {
                    with(value[0]) {
                        Text(
                            modifier = Modifier.padding(bottom = 6.dp),
                            text = "Time:  $time",
                            style = MaterialTheme.typography.subtitle1,
                        )
                        Text("temperature: %.2f".format(temperature))
                        Text("humidity: $humidity")
                    }
                }
            }
        }
    }
}

@Composable
private fun TitleRow(date: String, size: Int) {
    Text(
        text = "Readings from $date ($size)",
        fontSize = FontSize.large,
        modifier = Modifier.padding(start = 32.dp)
    )
}

@Composable
private fun HeadersRow() {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                start = 32.dp,
                bottom = 16.dp
            )
    ) {
        Text(
            text = stringResource(id = R.string.time),
            fontSize = FontSize.medium
        )
        Text(
            text = stringResource(id = R.string.temperature),
            fontSize = FontSize.medium
        )
        Text(
            text = stringResource(id = R.string.humidity),
            fontSize = FontSize.medium
        )
        Text(
            text = stringResource(id = R.string.sync),
            fontSize = FontSize.medium
        )
    }
}

@Composable
private fun WeatherReadingsList(
    weatherReadings: List<WeatherReading>,
    listState: LazyListState
) {
    LazyColumn(
        state = listState,
        verticalArrangement = Arrangement.spacedBy(6.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.padding(start = 32.dp)
    ) {
        items(weatherReadings) { weatherReading ->
            WeatherReadingItem(weatherReading = weatherReading)
        }
    }
}

@Composable
private fun WeatherReadingItem(weatherReading: WeatherReading) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier
            .fillMaxWidth()
    ) {
        with(weatherReading) {
            Text(
                text = time,
                fontSize = FontSize.medium
            )
            Text(
                text = "%.2f".format(temperature),
                fontSize = FontSize.medium
            )
            Text(
                text = humidity.toString(),
                fontSize = FontSize.medium
            )
            Image(
                painter = syncIconPainter(isSentToServer = isSentToServer),
                contentDescription = null
            )
        }
    }
}

@Composable
private fun syncIconPainter(isSentToServer: Boolean): Painter {
    return if (isSentToServer)
        painterResource(id = R.drawable.ic_sync)
    else
        painterResource(id = R.drawable.ic_sync_off)
}

@Composable
private fun Plot(
    weatherReadings: List<WeatherReading>,
    listState: LazyListState,
    visibility: MutableState<Boolean>,
    xOffset: MutableState<Float>,
    cardWidth: MutableState<Int>,
    reading: MutableState<List<WeatherReading>>,
    totalWidth: MutableState<Int>
) {
    val scope = rememberCoroutineScope()
    if (weatherReadings.isNotEmpty())
        LineGraph(
            plot = LinePlot(
                listOf(
                    LinePlot.Line(
                        dataPoints = weatherReadings.map {
                            DataPoint(
                                x = weatherReadings.indexOf(it).toFloat(),
                                y = it.temperature
                            )
                        },
                        connection = LinePlot.Connection(color = Color.Red),
                        intersection = LinePlot.Intersection(color = Color.Blue),
                        highlight = LinePlot.Highlight(color = Color.Yellow),
                    ),
                    LinePlot.Line(
                        dataPoints = weatherReadings.map {
                            DataPoint(
                                x = weatherReadings.indexOf(it).toFloat(),
                                y = it.humidity.toFloat()
                            )
                        },
                        connection = LinePlot.Connection(color = Color.Gray),
                        intersection = LinePlot.Intersection(color = Color.Black),
                        highlight = LinePlot.Highlight(color = Color.Yellow)
                    )
                ),
                selection = LinePlot.Selection(
                    highlight = LinePlot.Connection(
                        Color.Green,
                        strokeWidth = 2.dp,
                        pathEffect = PathEffect.dashPathEffect(floatArrayOf(40f, 20f))
                    )
                ),
                grid = LinePlot.Grid(
                    color = Color.Black,
                    steps = 9,
                ),
                yAxis = LinePlot.YAxis(steps = 9),
                xAxis = LinePlot.XAxis(
                    steps = weatherReadings.size / 5,
                    content = { _, _, _ ->
                        if (weatherReadings.size > 4)
                            for (it in 0 until weatherReadings.size / 5) {
                                val multipliedIndex = it + it * 5
                                val index = when {
                                    multipliedIndex >= weatherReadings.size -> weatherReadings.size - 1
                                    else -> multipliedIndex
                                }
                                if (it < weatherReadings.size / 5)
                                    Text(
                                        text = weatherReadings[index].time,
                                        style = MaterialTheme.typography.caption,
                                        color = MaterialTheme.colors.onSurface,
                                        textAlign = TextAlign.Center,
                                        modifier = Modifier.properPadding(it)
                                    )
                            }
                        else
                            Text(text = "")
                    }
                )
            ),
            modifier = Modifier.fillMaxHeight(0.5f),
            onSelectionStart = { visibility.value = true },
            onSelectionEnd = { visibility.value = false },
            onSelection = { xLine, points ->
                val index = points[0].x.toInt()
                scope.launch {
                    listState.animateScrollToItem(index)
                }
                val cWidth = cardWidth.value.toFloat()
                var xCenter = xLine
                xCenter = when {
                    xCenter + cWidth / 2f > totalWidth.value -> totalWidth.value - cWidth
                    xCenter - cWidth / 2f < 0f -> 0f
                    else -> xCenter - cWidth / 2f
                }
                xOffset.value = xCenter
                reading.value = listOf(weatherReadings[index])
            }
        )
}

@SuppressLint("UnnecessaryComposedModifier")
private fun Modifier.properPadding(index: Int) = composed {
    if (index == 0)
        padding(start = 36.dp)
    else Modifier
}

@Preview(showBackground = true)
@Composable
private fun ReadingsDetailsScreenPreview() {
    ReadingDetailsScreen(processor = previewProcessor(ReadingsDetailsState()))
}
