package com.untitledkingdom.bluetoothtrainer.feature.readingshistory.state

sealed interface ReadingsHistoryEvent {
    data class ItemClick(val date: String) : ReadingsHistoryEvent
}
