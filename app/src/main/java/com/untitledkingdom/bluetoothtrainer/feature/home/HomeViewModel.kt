package com.untitledkingdom.bluetoothtrainer.feature.home

import androidx.lifecycle.ViewModel
import com.tomcz.ellipse.EffectsCollector
import com.tomcz.ellipse.Processor
import com.tomcz.ellipse.common.processor
import com.tomcz.ellipse.common.toNoAction
import com.untitledkingdom.bluetoothtrainer.api.ConnectionState
import com.untitledkingdom.bluetoothtrainer.api.FakeInternetConnection
import com.untitledkingdom.bluetoothtrainer.bluetooth.BluetoothScanner
import com.untitledkingdom.bluetoothtrainer.datastore.DataStorage
import com.untitledkingdom.bluetoothtrainer.feature.home.data.AdvertisementView
import com.untitledkingdom.bluetoothtrainer.feature.home.state.HomeEffect
import com.untitledkingdom.bluetoothtrainer.feature.home.state.HomeEvent
import com.untitledkingdom.bluetoothtrainer.feature.home.state.HomePartialState
import com.untitledkingdom.bluetoothtrainer.feature.home.state.HomeState
import com.untitledkingdom.bluetoothtrainer.service.DeviceConnectionProcessorContainer
import com.untitledkingdom.bluetoothtrainer.service.state.DeviceConnectionEvent
import com.untitledkingdom.bluetoothtrainer.utils.Constants
import com.untitledkingdom.bluetoothtrainer.utils.peripheral.PeripheralProvider
import com.untitledkingdom.bluetoothtrainer.utils.peripheral.PeripheralState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.merge
import javax.inject.Inject

typealias HomeProcessor = Processor<HomeEvent, HomeState, HomeEffect>

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val bluetoothScanner: BluetoothScanner,
    private val dataStorage: DataStorage,
    private val peripheralProvider: PeripheralProvider,
    private val processorContainer: DeviceConnectionProcessorContainer,
    private val fakeInternetConnection: FakeInternetConnection
) : ViewModel() {
    private val foundAdvertisementsMap = HashMap<String, AdvertisementView>()
    val processor: HomeProcessor = processor(
        initialState = HomeState(),
        prepare = {
            merge(
                checkConnectedPeripheral(),
                checkIfDeviceIsConnected(),
                checkFakeInternetConnection()
            )
        },
        onEvent = { event ->
            when (event) {
                HomeEvent.StartScanningClick -> startScanning()
                HomeEvent.DisconnectPeripheral -> disconnect().toNoAction()
                is HomeEvent.UpdateBluetoothStatus -> flowOf(
                    HomePartialState.SetBluetoothEnablement(
                        isBluetoothEnabled = event.isBluetoothEnabled
                    )
                )
                is HomeEvent.UpdateLocationStatus -> flowOf(
                    HomePartialState.SetLocationEnablement(
                        isLocationEnabled = event.isLocationEnabled
                    )
                )
                is HomeEvent.UpdateFakeInternetStatus -> updateFakeInternetStatus(
                    isFakeInternetEnabled = event.isFakeInternetEnabled
                ).toNoAction()

                is HomeEvent.AdvertisementClick -> handleAdvertisementClick(
                    address = event.address,
                    effects = effects
                ).toNoAction()
            }
        }
    )

    private fun updateFakeInternetStatus(isFakeInternetEnabled: Boolean) {
        when (isFakeInternetEnabled) {
            true -> fakeInternetConnection.turnOn()
            false -> fakeInternetConnection.turnOff()
        }
    }

    private suspend fun disconnect() {
        dataStorage.save(Constants.ADDRESS, "")
        peripheralProvider.disconnect()
    }

    private fun checkConnectedPeripheral(): Flow<HomePartialState> =
        dataStorage.observeAddress(key = Constants.ADDRESS).map { address ->
            HomePartialState.ConnectedPeripheralAddressChanged(address = address)
        }

    private fun checkIfDeviceIsConnected(): Flow<HomePartialState> =
        peripheralProvider.state.map { state ->
            when (state) {
                PeripheralState.Connected ->
                    HomePartialState.IsDeviceConnectedChanged(isDeviceConnected = true)
                else -> HomePartialState.IsDeviceConnectedChanged(isDeviceConnected = false)
            }
        }

    private suspend fun handleAdvertisementClick(
        address: String,
        effects: EffectsCollector<HomeEffect>
    ) {
        dataStorage.save(key = Constants.ADDRESS, address)
        processorContainer.processor.sendEvent(DeviceConnectionEvent.InitializePeripheral)
        effects.send(HomeEffect.GoToReadingsHistory(address = address))
    }

    private fun startScanning(): Flow<HomePartialState> =
        bluetoothScanner.scanAdvertisements()
            .map { advertisement ->
                foundAdvertisementsMap[advertisement.address] = advertisement
                HomePartialState.AdvertisementListChanged(
                    foundAdvertisementsMap.values
                        .toList()
                        .filter { it.name != "no name" }
                )
            }

    private fun checkFakeInternetConnection(): Flow<HomePartialState> =
        fakeInternetConnection.state.map { connectionState ->
            when (connectionState) {
                ConnectionState.On -> HomePartialState.SetFakeInternetEnablement(
                    isFakeInternetEnabled = true
                )
                ConnectionState.Off -> HomePartialState.SetFakeInternetEnablement(
                    isFakeInternetEnabled = false
                )
            }
        }
}
