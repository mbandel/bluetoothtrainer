package com.untitledkingdom.bluetoothtrainer.feature.deviceinfo

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import com.tomcz.ellipse.common.onProcessor
import com.untitledkingdom.bluetoothtrainer.service.DeviceConnectionService
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.FlowPreview

@AndroidEntryPoint
@FlowPreview
@DelicateCoroutinesApi
class DeviceInfoFragment : Fragment() {
    private val viewModel: DeviceInfoViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        onProcessor(
            lifecycleState = Lifecycle.State.RESUMED,
            processor = { viewModel.processor },
        )
        requireContext().startService(
            Intent(requireContext(), DeviceConnectionService::class.java)
        )
        return ComposeView(requireContext()).apply {
            setContent {
                DeviceInfoScreen(processor = viewModel.processor)
            }
        }
    }
}
