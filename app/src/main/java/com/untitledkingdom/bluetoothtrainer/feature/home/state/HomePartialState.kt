package com.untitledkingdom.bluetoothtrainer.feature.home.state

import com.tomcz.ellipse.PartialState
import com.untitledkingdom.bluetoothtrainer.feature.home.data.AdvertisementView

sealed interface HomePartialState : PartialState<HomeState> {
    data class SetBluetoothEnablement(val isBluetoothEnabled: Boolean) : HomePartialState {
        override fun reduce(oldState: HomeState): HomeState =
            oldState.copy(isBluetoothEnabled = isBluetoothEnabled)
    }

    data class SetLocationEnablement(val isLocationEnabled: Boolean) : HomePartialState {
        override fun reduce(oldState: HomeState): HomeState =
            oldState.copy(isLocationEnabled = isLocationEnabled)
    }

    data class SetFakeInternetEnablement(val isFakeInternetEnabled: Boolean) : HomePartialState {
        override fun reduce(oldState: HomeState): HomeState =
            oldState.copy(isFakeInternetEnabled = isFakeInternetEnabled)
    }

    data class AdvertisementListChanged(val advertisements: List<AdvertisementView>) :
        HomePartialState {
        override fun reduce(oldState: HomeState): HomeState =
            oldState.copy(advertisements = advertisements)
    }

    data class IsDeviceConnectedChanged(val isDeviceConnected: Boolean) : HomePartialState {
        override fun reduce(oldState: HomeState): HomeState =
            oldState.copy(isDeviceConnected = isDeviceConnected)
    }

    data class ConnectedPeripheralAddressChanged(val address: String) : HomePartialState {
        override fun reduce(oldState: HomeState): HomeState =
            oldState.copy(connectedPeripheralAddress = address)
    }
}
