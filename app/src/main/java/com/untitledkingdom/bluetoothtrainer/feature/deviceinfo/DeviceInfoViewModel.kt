package com.untitledkingdom.bluetoothtrainer.feature.deviceinfo

import androidx.lifecycle.ViewModel
import com.tomcz.ellipse.Processor
import com.tomcz.ellipse.common.processor
import com.untitledkingdom.bluetoothtrainer.feature.deviceinfo.state.DeviceInfoEffect
import com.untitledkingdom.bluetoothtrainer.feature.deviceinfo.state.DeviceInfoEvent
import com.untitledkingdom.bluetoothtrainer.feature.deviceinfo.state.DeviceInfoState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.flowOf
import javax.inject.Inject

typealias DeviceInfoProcessor = Processor<DeviceInfoEvent, DeviceInfoState, DeviceInfoEffect>

@HiltViewModel
class DeviceInfoViewModel @Inject constructor() : ViewModel() {
    val processor: DeviceInfoProcessor = processor(
        initialState = DeviceInfoState(),
        onEvent = { event ->
            when (event) {
                is DeviceInfoEvent.InitializePeripheral -> flowOf()
            }
        }
    )
}
