package com.untitledkingdom.bluetoothtrainer.feature.readingsDetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import com.tomcz.ellipse.common.onProcessor
import com.untitledkingdom.bluetoothtrainer.feature.readingsDetails.state.ReadingsDetailsEvent
import com.untitledkingdom.bluetoothtrainer.utils.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.FlowPreview

@AndroidEntryPoint
@FlowPreview
class ReadingsDetailsFragment : Fragment() {
    private val viewModel: ReadingsDetailsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        onProcessor(
            lifecycleState = Lifecycle.State.RESUMED,
            processor = { viewModel.processor }
        )
        val date = arguments?.getString(Constants.DATE)
        date?.let {
            viewModel.processor.sendEvent(ReadingsDetailsEvent.FetchWeatherReadings(date = it))
        }
        return ComposeView(requireContext()).apply {
            setContent {
                ReadingDetailsScreen(processor = viewModel.processor)
            }
        }
    }
}
