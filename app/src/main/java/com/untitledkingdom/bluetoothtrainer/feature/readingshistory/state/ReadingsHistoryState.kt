package com.untitledkingdom.bluetoothtrainer.feature.readingshistory.state

import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.data.DailyReadingsInfo

data class ReadingsHistoryState(
    val dailyReadingsInfoList: List<DailyReadingsInfo> = listOf(),
)
