package com.untitledkingdom.bluetoothtrainer.feature.readingshistory.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.untitledkingdom.bluetoothtrainer.utils.Constants

@Entity(tableName = Constants.WEATHER_TABLE)
data class WeatherReading(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val temperature: Float,
    val humidity: Int,
    val date: String,
    val time: String,
    val isSentToServer: Boolean = false
)
