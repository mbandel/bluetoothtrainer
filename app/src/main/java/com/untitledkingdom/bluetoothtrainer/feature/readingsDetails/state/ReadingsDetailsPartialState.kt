package com.untitledkingdom.bluetoothtrainer.feature.readingsDetails.state

import com.tomcz.ellipse.PartialState
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.data.WeatherReading

sealed interface ReadingsDetailsPartialState : PartialState<ReadingsDetailsState> {
    data class WeatherReadingsChanged(
        val weatherReadings: List<WeatherReading>
    ) : ReadingsDetailsPartialState {
        override fun reduce(oldState: ReadingsDetailsState): ReadingsDetailsState =
            oldState.copy(weatherReadings = weatherReadings)
    }

    data class SetDate(val date: String) : ReadingsDetailsPartialState {
        override fun reduce(oldState: ReadingsDetailsState): ReadingsDetailsState =
            oldState.copy(date = date)
    }
}
