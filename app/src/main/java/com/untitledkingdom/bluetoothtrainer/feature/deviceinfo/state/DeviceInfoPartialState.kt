package com.untitledkingdom.bluetoothtrainer.feature.deviceinfo.state

import com.juul.kable.Peripheral
import com.tomcz.ellipse.PartialState

sealed interface DeviceInfoPartialState : PartialState<DeviceInfoState> {
    data class SetPeripheral(val peripheral: Peripheral) : DeviceInfoPartialState {
        override fun reduce(oldState: DeviceInfoState): DeviceInfoState =
            oldState.copy(peripheral = peripheral)
    }

    data class SetDate(val date: String) : DeviceInfoPartialState {
        override fun reduce(oldState: DeviceInfoState): DeviceInfoState =
            oldState.copy(date = date)
    }
}
