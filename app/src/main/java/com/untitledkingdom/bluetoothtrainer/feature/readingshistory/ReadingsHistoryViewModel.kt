package com.untitledkingdom.bluetoothtrainer.feature.readingshistory

import androidx.lifecycle.ViewModel
import com.tomcz.ellipse.Processor
import com.tomcz.ellipse.common.processor
import com.tomcz.ellipse.common.toNoAction
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.state.ReadingsHistoryEffect
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.state.ReadingsHistoryEvent
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.state.ReadingsHistoryPartialState
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.state.ReadingsHistoryState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.map
import javax.inject.Inject

typealias ReadingHistoryProcessor =
    Processor<ReadingsHistoryEvent, ReadingsHistoryState, ReadingsHistoryEffect>

@HiltViewModel
class ReadingsHistoryViewModel @Inject constructor(
    private val repository: ReadingsHistoryRepository
) : ViewModel() {
    val processor: ReadingHistoryProcessor = processor(
        initialState = ReadingsHistoryState(),
        prepare = { observeWeatherReadingsInfo() },
        onEvent = { event ->
            when (event) {
                is ReadingsHistoryEvent.ItemClick ->
                    effects.send(
                        ReadingsHistoryEffect.GoToWeatherReadingsDetails(event.date)
                    ).toNoAction()
            }
        }
    )

    private fun observeWeatherReadingsInfo() =
        repository.observeReadingsInfo().map { readingsInfoList ->
            ReadingsHistoryPartialState.DailyReadingsInfoListChanged(
                dailyReadingsInfoList = readingsInfoList.sortedBy { it.date }
            )
        }
}
