package com.untitledkingdom.bluetoothtrainer.feature.home

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import com.tomcz.ellipse.common.onProcessor
import com.untitledkingdom.bluetoothtrainer.R
import com.untitledkingdom.bluetoothtrainer.feature.home.state.HomeEffect
import com.untitledkingdom.bluetoothtrainer.feature.home.state.HomeEvent
import com.untitledkingdom.bluetoothtrainer.utils.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.FlowPreview

@FlowPreview
@AndroidEntryPoint
class HomeFragment : Fragment() {
    private val viewModel: HomeViewModel by viewModels()

    private val bluetoothAdapter: BluetoothAdapter by lazy {
        val bluetoothManager = requireContext().getSystemService(
            Context.BLUETOOTH_SERVICE
        ) as BluetoothManager
        bluetoothManager.adapter
    }
    private val bluetoothBroadcastReceiver: BroadcastReceiver = provideBluetoothBroadcastReceiver()
    private val locationBroadcastReceiver = provideLocationBroadcastReceiver()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        onProcessor(
            lifecycleState = Lifecycle.State.STARTED,
            processor = { viewModel.processor },
            onEffect = ::trigger
        )

        return ComposeView(
            requireContext()
        ).apply {
            setContent {
                HomeScreen(processor = viewModel.processor)
            }
        }
    }

    private fun trigger(effect: HomeEffect) {
        when (effect) {
            HomeEffect.AskForPermissions -> {
            }
            is HomeEffect.GoToReadingsHistory -> findNavController().navigate(
                resId = R.id.readingsHistoryFragment,
                args = bundleOf(
                    Constants.ADDRESS to effect.address
                )
            )
        }
    }

    override fun onResume() {
        super.onResume()
        val locationManager = requireContext().getSystemService(
            Context.LOCATION_SERVICE
        ) as LocationManager
        viewModel.processor.sendEvent(
            HomeEvent.UpdateBluetoothStatus(
                isBluetoothEnabled = bluetoothAdapter.isEnabled
            )
        )
        viewModel.processor.sendEvent(
            HomeEvent.UpdateLocationStatus(
                isLocationEnabled = locationManager.isLocationEnabled
            )
        )
        registerReceivers()
    }

    override fun onPause() {
        super.onPause()
        unregisterReceivers()
    }

    private fun registerReceivers() {
        requireContext().registerReceiver(
            bluetoothBroadcastReceiver,
            IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        )
        requireContext().registerReceiver(
            locationBroadcastReceiver,
            IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION)
        )
    }

    private fun unregisterReceivers() {
        requireContext().unregisterReceiver(bluetoothBroadcastReceiver)
        requireContext().unregisterReceiver(locationBroadcastReceiver)
    }

    private fun provideLocationBroadcastReceiver(): BroadcastReceiver =
        object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                val action: String? = intent?.action
                if (action.equals(LocationManager.PROVIDERS_CHANGED_ACTION)) {
                    val locationManager = requireContext().getSystemService(
                        Context.LOCATION_SERVICE
                    ) as LocationManager
                    if (locationManager.isLocationEnabled) {
                        viewModel.processor.sendEvent(
                            HomeEvent.UpdateLocationStatus(
                                isLocationEnabled = true
                            )
                        )
                    } else {
                        viewModel.processor.sendEvent(
                            HomeEvent.UpdateLocationStatus(
                                isLocationEnabled = false
                            )
                        )
                    }
                }
            }
        }

    private fun provideBluetoothBroadcastReceiver(): BroadcastReceiver =
        object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                val action: String? = intent?.action
                if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                    val state: Int? =
                        intent?.getIntExtra(
                            BluetoothAdapter.EXTRA_STATE,
                            BluetoothAdapter.ERROR
                        )
                    when (state) {
                        BluetoothAdapter.STATE_ON -> viewModel.processor.sendEvent(
                            HomeEvent.UpdateBluetoothStatus(
                                isBluetoothEnabled = true
                            )
                        )
                        BluetoothAdapter.STATE_OFF -> {
                            viewModel.processor.sendEvent(
                                HomeEvent.UpdateBluetoothStatus(
                                    isBluetoothEnabled = false
                                )
                            )
                        }
                    }
                }
            }
        }
}
