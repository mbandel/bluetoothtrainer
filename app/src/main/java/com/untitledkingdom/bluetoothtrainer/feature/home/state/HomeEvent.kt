package com.untitledkingdom.bluetoothtrainer.feature.home.state

sealed interface HomeEvent {
    object StartScanningClick : HomeEvent
    object DisconnectPeripheral : HomeEvent
    data class UpdateBluetoothStatus(val isBluetoothEnabled: Boolean) : HomeEvent
    data class UpdateLocationStatus(val isLocationEnabled: Boolean) : HomeEvent
    data class UpdateFakeInternetStatus(val isFakeInternetEnabled: Boolean) : HomeEvent
    data class AdvertisementClick(val address: String) : HomeEvent
}
