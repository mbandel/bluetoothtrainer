package com.untitledkingdom.bluetoothtrainer.feature.readingshistory

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import com.tomcz.ellipse.common.onProcessor
import com.untitledkingdom.bluetoothtrainer.R
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.state.ReadingsHistoryEffect
import com.untitledkingdom.bluetoothtrainer.utils.Constants
import com.untitledkingdom.bluetoothtrainer.utils.findMainNavigationController
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.FlowPreview

@AndroidEntryPoint
@FlowPreview
@DelicateCoroutinesApi
class ReadingsHistoryFragment : Fragment() {
    private val viewModel: ReadingsHistoryViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        onProcessor(
            lifecycleState = Lifecycle.State.STARTED,
            processor = { viewModel.processor },
            onEffect = ::trigger
        )
        return ComposeView(
            requireContext()
        ).apply {
            setContent {
                ReadingsHistoryScreen(processor = viewModel.processor)
            }
        }
    }

    private fun trigger(effect: ReadingsHistoryEffect) {
        when (effect) {
            is ReadingsHistoryEffect.GoToWeatherReadingsDetails -> goToReadingsDetails(effect.date)
        }
    }

    private fun goToReadingsDetails(date: String) {
        findMainNavigationController().navigate(
            resId = R.id.readingsDetailsFragment,
            args = bundleOf(
                Constants.DATE to date
            )
        )
    }
}
