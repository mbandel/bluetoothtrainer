package com.untitledkingdom.bluetoothtrainer.feature.navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import by.kirich1409.viewbindingdelegate.CreateMethod
import by.kirich1409.viewbindingdelegate.viewBinding
import com.untitledkingdom.bluetoothtrainer.R
import com.untitledkingdom.bluetoothtrainer.databinding.FragmentNavigationBinding

class NavigationFragment : Fragment() {
    private val binding: FragmentNavigationBinding by viewBinding(
        createMethod = CreateMethod.INFLATE
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initBottomNavigation()
    }

    private fun initBottomNavigation() {
        val navHost: NavHostFragment =
            childFragmentManager.findFragmentById(R.id.fragment_container_view) as NavHostFragment
        val bottomNavigationView = binding.bottomNavigationView
        val navController = navHost.findNavController()
        bottomNavigationView.setupWithNavController(navController)
    }
}
