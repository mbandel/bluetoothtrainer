package com.untitledkingdom.bluetoothtrainer.feature.readingshistory.state

import com.tomcz.ellipse.PartialState
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.data.DailyReadingsInfo

sealed interface ReadingsHistoryPartialState : PartialState<ReadingsHistoryState> {
    data class DailyReadingsInfoListChanged(
        val dailyReadingsInfoList: List<DailyReadingsInfo>
    ) : ReadingsHistoryPartialState {
        override fun reduce(oldState: ReadingsHistoryState): ReadingsHistoryState =
            oldState.copy(dailyReadingsInfoList = dailyReadingsInfoList)
    }
}
