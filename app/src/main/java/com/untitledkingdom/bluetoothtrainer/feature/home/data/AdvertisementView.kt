package com.untitledkingdom.bluetoothtrainer.feature.home.data

import com.juul.kable.Advertisement

data class AdvertisementView(
    val address: String,
    val rssi: Int,
    val name: String,
)

fun Advertisement.toAdvertisementView(): AdvertisementView =
    AdvertisementView(
        address = address,
        name = name ?: "no name",
        rssi = rssi
    )
