package com.untitledkingdom.bluetoothtrainer.feature.deviceinfo

import com.benasher44.uuid.uuidFrom
import com.juul.kable.Characteristic
import com.juul.kable.Peripheral

object Characteristics {
    private const val dateServiceUUID = "fd8136b0-f18f-4f36-ad03-c73311525a80"
    private const val dateCharacteristicUUID = "fd8136b1-f18f-4f36-ad03-c73311525a80"
    private const val weatherServiceUUID = "fd8136c0-f18f-4f36-ad03-c73311525a80"
    private const val weatherCharacteristicUUID = "fd8136c1-f18f-4f36-ad03-c73311525a80"
    private const val batteryServiceUUID = "fd8136d0-f18f-4f36-ad03-c73311525a80"
    private const val batteryCharacteristicUUID = "fd8136d1-f18f-4f36-ad03-c73311525a80"

    fun date(peripheral: Peripheral): Characteristic {
        val services = peripheral.services ?: error("Services have not been discovered")
        return services
            .first { it.serviceUuid == uuidFrom(dateServiceUUID) }
            .characteristics
            .first { it.characteristicUuid == uuidFrom(dateCharacteristicUUID) }
    }

    fun weather(peripheral: Peripheral): Characteristic {
        val services = peripheral.services ?: error("Services have not been discovered")
        return services
            .first { it.serviceUuid == uuidFrom(weatherServiceUUID) }
            .characteristics
            .first { it.characteristicUuid == uuidFrom(weatherCharacteristicUUID) }
    }

    fun battery(peripheral: Peripheral): Characteristic {
        val services = peripheral.services ?: error("Services have not been discovered")
        return services
            .first { it.serviceUuid == uuidFrom(batteryServiceUUID) }
            .characteristics
            .first { it.characteristicUuid == uuidFrom(batteryCharacteristicUUID) }
    }
}
