package com.untitledkingdom.bluetoothtrainer.feature.deviceinfo.state

import com.juul.kable.Peripheral

data class DeviceInfoState(
    val peripheral: Peripheral? = null,
    val date: String = ""
)
