package com.untitledkingdom.bluetoothtrainer.feature.readingshistory

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.* // ktlint-disable no-wildcard-imports
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.tomcz.ellipse.common.collectAsState
import com.tomcz.ellipse.common.previewProcessor
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.data.DailyReadingsInfo
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.state.ReadingsHistoryEvent
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.state.ReadingsHistoryState
import com.untitledkingdom.bluetoothtrainer.ui.Colors
import com.untitledkingdom.bluetoothtrainer.ui.FontSize

@Composable
fun ReadingsHistoryScreen(processor: ReadingHistoryProcessor) {
    val readingsInfoList by processor.collectAsState { it.dailyReadingsInfoList }
    Column {
        ReadingsInfoTitle()
        ReadingsInfoList(
            weatherReadingsInfoList = readingsInfoList,
            onItemClick = { processor.sendEvent(ReadingsHistoryEvent.ItemClick(it)) }
        )
    }
}

@Composable
private fun ReadingsInfoList(
    weatherReadingsInfoList: List<DailyReadingsInfo>,
    onItemClick: (date: String) -> Unit
) {
    LazyColumn {
        items(weatherReadingsInfoList) { dailyReadingsInfo ->
            Column(
                verticalArrangement = Arrangement.spacedBy(6.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                ReadingsInfo(
                    dailyReadingsInfo = dailyReadingsInfo,
                    onItemClick = onItemClick
                )
                DividerGray()
            }
        }
    }
}

@Composable
private fun ReadingsInfoTitle() {
    Column {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 24.dp)
        ) {
            Text(
                text = "date",
                modifier = Modifier.weight(0.3f)
            )
            Text(
                text = "min temp",
                modifier = Modifier.weight(0.15f)
            )
            Text(
                text = "max temp",
                modifier = Modifier.weight(0.15f)
            )
            Text(
                text = "avg temp",
                modifier = Modifier.weight(0.15f)
            )
            Text(
                text = "mim hum",
                modifier = Modifier.weight(0.12f)
            )
            Text(
                text = "max hum",
                modifier = Modifier.weight(0.12f)
            )
            Text(
                text = "avg hum",
                modifier = Modifier.weight(0.15f)
            )
        }
        DividerGray(
            modifier = Modifier.padding(
                bottom = 12.dp,
                top = 12.dp
            )
        )
    }
}

@Composable
private fun ReadingsInfo(
    dailyReadingsInfo: DailyReadingsInfo,
    onItemClick: (date: String) -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                horizontal = 24.dp,
                vertical = 8.dp
            )
            .clickable { onItemClick(dailyReadingsInfo.date) },
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        with(dailyReadingsInfo) {
            Text(
                text = date,
                fontSize = FontSize.medium,
                modifier = Modifier.weight(0.3f)
            )
            Text(
                text = minTemperature.toStringFormatted(),
                fontSize = FontSize.medium,
                modifier = Modifier.weight(0.15f)
            )
            Text(
                text = maxTemperature.toStringFormatted(),
                fontSize = FontSize.medium,
                modifier = Modifier.weight(0.15f)
            )
            Text(
                text = avgTemperature.toStringFormatted(),
                fontSize = FontSize.medium,
                modifier = Modifier.weight(0.15f)
            )
            Text(
                text = minHumidity.toString(),
                fontSize = FontSize.medium,
                modifier = Modifier.weight(0.12f)
            )
            Text(
                text = maxHumidity.toString(),
                fontSize = FontSize.medium,
                modifier = Modifier.weight(0.12f)
            )
            Text(
                text = avgHumidity.toStringFormatted(),
                fontSize = FontSize.medium,
                modifier = Modifier.weight(0.15f)
            )
        }
    }
}

@Composable
fun DividerGray(modifier: Modifier = Modifier) {
    Divider(
        modifier = modifier.fillMaxWidth(),
        thickness = 1.dp,
        color = Colors.gray
    )
}

private fun Float.toStringFormatted(): String =
    "%.2f".format(this)

@Preview(showBackground = true)
@Composable
private fun ReadingHistoryScreenPreview() {
    ReadingsHistoryScreen(processor = previewProcessor(ReadingsHistoryState()))
}
