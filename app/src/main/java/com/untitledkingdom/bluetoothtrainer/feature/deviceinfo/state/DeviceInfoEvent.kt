package com.untitledkingdom.bluetoothtrainer.feature.deviceinfo.state

sealed interface DeviceInfoEvent {
    data class InitializePeripheral(val address: String) : DeviceInfoEvent
}
