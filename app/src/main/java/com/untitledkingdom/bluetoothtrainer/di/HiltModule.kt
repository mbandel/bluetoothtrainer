package com.untitledkingdom.bluetoothtrainer.di

import android.app.Application
import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.room.Room
import com.untitledkingdom.bluetoothtrainer.api.ApiService
import com.untitledkingdom.bluetoothtrainer.api.ApiServiceImpl
import com.untitledkingdom.bluetoothtrainer.bluetooth.BluetoothScanner
import com.untitledkingdom.bluetoothtrainer.bluetooth.BluetoothScannerImpl
import com.untitledkingdom.bluetoothtrainer.database.Database
import com.untitledkingdom.bluetoothtrainer.datastore.DataStorage
import com.untitledkingdom.bluetoothtrainer.datastore.DataStorageImpl
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.ReadingsHistoryRepository
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.ReadingsHistoryRepositoryImpl
import com.untitledkingdom.bluetoothtrainer.utils.Constants
import com.untitledkingdom.bluetoothtrainer.utils.Constants.DATA_STORE_NAME
import com.untitledkingdom.bluetoothtrainer.utils.time.TimeProvider
import com.untitledkingdom.bluetoothtrainer.utils.time.TimeProviderImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface BindsModule {
    @Binds
    fun bindBluetoothService(
        bluetoothScannerImpl: BluetoothScannerImpl
    ): BluetoothScanner

    @Binds
    fun bindDataStorage(dataStorageImpl: DataStorageImpl): DataStorage

    @Binds
    fun bindTimeProvider(timeProviderImpl: TimeProviderImpl): TimeProvider

    @Binds
    fun bindReadingsHistoryRepository(
        readingsHistoryRepositoryImpl: ReadingsHistoryRepositoryImpl
    ): ReadingsHistoryRepository

    @Binds
    fun bindApiService(apiServiceImpl: ApiServiceImpl): ApiService
}

@Module
@InstallIn(SingletonComponent::class)
object ProvidesModule {
    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(
        name = DATA_STORE_NAME
    )

    @Provides
    @Singleton
    fun provideDataStore(context: Application): DataStore<Preferences> {
        return context.dataStore
    }

    @Provides
    @Singleton
    fun provideDatabase(context: Application): Database =
        Room.databaseBuilder(
            context,
            Database::class.java,
            Constants.DATABASE_NAME,
        )
            .fallbackToDestructiveMigration()
            .build()
}
