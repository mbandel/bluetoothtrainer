package com.untitledkingdom.bluetoothtrainer.utils

import java.nio.ByteBuffer
import java.time.LocalDate

object ByteConverter {
    fun getDateFromByteArray(array: ByteArray): LocalDate {
        val year = (array[3].toInt() and 0xFF shl 8) + (array[2].toInt() and 0xFF)
        return LocalDate.of(year, array[1].toInt(), array[0].toInt())
    }

    fun getByteArrayFromDate(date: LocalDate): ByteArray {
        val yearBuffer: ByteBuffer = ByteBuffer.allocate(2)
        yearBuffer.putShort(date.year.toShort())
        return byteArrayOf(
            date.dayOfMonth.toByte(),
            date.monthValue.toByte(),
            yearBuffer[1],
            yearBuffer[0]
        )
    }
}
