package com.untitledkingdom.bluetoothtrainer.utils.peripheral

import com.juul.kable.Peripheral
import com.juul.kable.State
import com.juul.kable.WriteType
import com.juul.kable.peripheral
import com.untitledkingdom.bluetoothtrainer.database.Database
import com.untitledkingdom.bluetoothtrainer.datastore.DataStorage
import com.untitledkingdom.bluetoothtrainer.feature.deviceinfo.Characteristics
import com.untitledkingdom.bluetoothtrainer.feature.readingshistory.data.WeatherReading
import com.untitledkingdom.bluetoothtrainer.utils.ByteConverter
import com.untitledkingdom.bluetoothtrainer.utils.Constants
import com.untitledkingdom.bluetoothtrainer.utils.time.TimeProvider
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.flow.* // ktlint-disable no-wildcard-imports
import java.time.LocalDate
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PeripheralProvider @Inject constructor(
    private val database: Database,
    private val timeProvider: TimeProvider,
    private val dataStorage: DataStorage
) {
    private lateinit var _peripheral: Peripheral
    val peripheral: Peripheral
        get() = _peripheral

    private val _state: MutableStateFlow<PeripheralState> =
        MutableStateFlow(PeripheralState.Disconnected)
    val state: StateFlow<PeripheralState>
        get() = _state

    private val _batteryLevel: MutableStateFlow<BatteryLevel> = MutableStateFlow(BatteryLevel.High)
    val batteryLevel: StateFlow<BatteryLevel>
        get() = _batteryLevel

    private val scope: CoroutineScope = CoroutineScope(
        SupervisorJob() + Dispatchers.Main.immediate
    )

    fun setDisconnected() {
        _state.value = PeripheralState.Disconnected
    }

    suspend fun disconnect() {
        _peripheral.disconnect()
    }

    suspend fun init(address: String) {
        _peripheral = scope.peripheral(address)
        observeWeather()
        observeBatteryLevel()
        enableAutoReconnect()
    }

    suspend fun cancelScope() {
        _peripheral.disconnect()
        scope.coroutineContext.cancelChildren()
    }

    private suspend fun enableAutoReconnect() {
        try {
            peripheral.state.collect { state ->
                when (state) {
                    State.Connected -> {
                        _state.value = PeripheralState.Connected
                        println("*** Connected ***")
                        writeCurrentDate(peripheral)
                        println(
                            ByteConverter.getDateFromByteArray(
                                readDateByteArray(peripheral = peripheral)
                            )
                        )
                    }
                    is State.Disconnected -> {
                        _state.value = PeripheralState.Disconnected
                        delay(500)
                        if (dataStorage.observeAddress(Constants.ADDRESS).first() != "")
                            peripheral.connect()
                        println("*** Disconnected ***")
                    }
                    else -> {}
                }
            }
        } catch (e: Exception) {
            peripheral.disconnect()
            enableAutoReconnect()
        }
    }

    private suspend fun readDateByteArray(peripheral: Peripheral): ByteArray {
        val dateCharacteristic = Characteristics.date(peripheral = peripheral)
        return peripheral.read(dateCharacteristic)
    }

    private suspend fun writeCurrentDate(peripheral: Peripheral) {
        val date = LocalDate.now()
        peripheral.write(
            characteristic = Characteristics.date(peripheral = peripheral),
            data = ByteConverter.getByteArrayFromDate(date = date),
            writeType = WriteType.WithResponse
        )
    }

    private fun observeWeather() = scope.launch {
        state.collectLatest { state ->
            when (state) {
                is PeripheralState.Connected -> peripheral.observe(
                    characteristic = Characteristics.weather(peripheral = peripheral)
                ).collect { data ->
                    val reading = ReadingsOuterClass.Readings.parseFrom(data)
                    database.getWeatherDao().addWeatherReading(
                        WeatherReading(
                            temperature = reading.temperature,
                            humidity = reading.hummidity,
                            date = timeProvider.getFormattedDate(),
                            time = timeProvider.getFormattedTime()
                        )
                    )
                }
                is PeripheralState.Disconnected -> {}
            }
        }
    }

    private fun observeBatteryLevel() = scope.launch {
        state.collect { state ->
            when (state) {
                PeripheralState.Connected -> {
                    peripheral.observe(
                        characteristic = Characteristics.battery(peripheral = peripheral)
                    ).collect { data ->
                        val batteryLevel = data[0].toShort()
                        if (batteryLevel < 20)
                            _batteryLevel.value = BatteryLevel.Low
                        else _batteryLevel.value = BatteryLevel.High
                    }
                }
                else -> {}
            }
        }
    }
}
