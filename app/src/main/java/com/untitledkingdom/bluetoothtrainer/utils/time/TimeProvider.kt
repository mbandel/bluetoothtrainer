package com.untitledkingdom.bluetoothtrainer.utils.time

import java.time.LocalDateTime

interface TimeProvider {
    fun getCurrentDateTime(): LocalDateTime
    fun getFormattedDate(): String
    fun getFormattedTime(): String
}
