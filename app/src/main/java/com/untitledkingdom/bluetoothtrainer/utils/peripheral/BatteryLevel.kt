package com.untitledkingdom.bluetoothtrainer.utils.peripheral

sealed class BatteryLevel {
    object Low : BatteryLevel()
    object High : BatteryLevel()
}
