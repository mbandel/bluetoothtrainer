package com.untitledkingdom.bluetoothtrainer.utils

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.untitledkingdom.bluetoothtrainer.R

fun Fragment.findMainNavigationController(): NavController {
    val appCompatActivity = this.requireActivity() as AppCompatActivity
    val navHostFragment =
        appCompatActivity.supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
            as NavHostFragment
    return navHostFragment.navController
}
