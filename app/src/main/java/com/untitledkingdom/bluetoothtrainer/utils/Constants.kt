package com.untitledkingdom.bluetoothtrainer.utils

object Constants {
    const val SYNC_PACKAGE_SIZE = 20
    const val ADDRESS = "address"
    const val DATA_STORE_NAME = "data_store"
    const val DATABASE_NAME = "database"
    const val DATE = "date"
    const val WEATHER_TABLE = "weather"
}
