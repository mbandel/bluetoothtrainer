package com.untitledkingdom.bluetoothtrainer.utils.peripheral

sealed class PeripheralState {
    object Connected : PeripheralState()
    object Disconnected : PeripheralState()
}
