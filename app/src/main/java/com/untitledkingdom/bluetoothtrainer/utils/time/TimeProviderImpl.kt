package com.untitledkingdom.bluetoothtrainer.utils.time

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import javax.inject.Inject

class TimeProviderImpl @Inject constructor() : TimeProvider {
    private fun provideCurrentInstant(): Instant = Instant.now()
    private fun provideCurrentZone(): ZoneId = ZoneId.systemDefault()

    override fun getCurrentDateTime(): LocalDateTime =
        LocalDateTime.ofInstant(provideCurrentInstant(), provideCurrentZone())

    override fun getFormattedDate(): String =
        LocalDateTime.ofInstant(provideCurrentInstant(), provideCurrentZone())
            .format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))

    override fun getFormattedTime(): String =
        LocalDateTime.ofInstant(provideCurrentInstant(), provideCurrentZone())
            .format(DateTimeFormatter.ofPattern("HH:mm:ss"))
}
