# README #
Bluetooth trainer -  application that collects data from device using bluetooth.
Bluetooth and location should be enabled for application to work correctly
Created by Maciej Bandel

### Technological stack ###

* kotlin
* MVI + ellipse
* Kotlin asynchronous bluetooth low energy
* Kotlin coroutines
* Jetpack Compose
* Room
* Datastore
* Mockk